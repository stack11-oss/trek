#!/bin/bash
set -e
docker build -t registry.gitlab.com/stack11-oss/trek/migra:latest - < Dockerfile.migra
docker build -t registry.gitlab.com/stack11-oss/trek:latest .
