module gitlab.com/stack11-oss/trek

go 1.16

require (
	github.com/fsnotify/fsnotify v1.5.1
	github.com/golang-migrate/migrate/v4 v4.15.0
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/lib/pq v1.10.3 // indirect
	github.com/spf13/cobra v1.2.1
	github.com/thecodeteam/goodbye v0.0.0-20170927022442-a83968bda2d3
	go.uber.org/atomic v1.9.0 // indirect
	golang.org/x/sys v0.0.0-20210923061019-b8560ed6a9b7 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
