package internal

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Config struct {
	ModelName     string   `yaml:"model_name"`
	DatabaseName  string   `yaml:"db_name"`
	DatabaseUsers []string `yaml:"db_users"`
}

func ReadConfig() (*Config, error) {
	var config *Config
	file, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(file, &config)
	if err != nil {
		return nil, err
	}
	return config, nil
}
