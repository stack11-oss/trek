package internal

import (
	"os"
	"os/exec"
	"strings"
)

const MIGRA_DOCKER_IMAGE = "registry.gitlab.com/stack11-oss/trek/migra:latest"

func Migra(from, to string) (string, error) {
	cmdMigra := exec.Command("docker", "run", "--rm", MIGRA_DOCKER_IMAGE, "migra", "--unsafe", "--with-privileges", from, to)
	cmdMigra.Stderr = os.Stderr
	output, err := cmdMigra.Output()
	if err != nil && cmdMigra.ProcessState.ExitCode() != 2 {
		return "", err
	}
	return strings.TrimSuffix(string(output), "\n"), nil
}
