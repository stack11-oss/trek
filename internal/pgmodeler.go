package internal

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
)

const PGMODELER_CLI_DOCKER_IMAGE = "geertjohan/pgmodeler-cli:latest"

func PgModelerExportToFile(input, output string) error {
	err := ioutil.WriteFile(output, []byte{}, 0755)
	if err != nil {
		return err
	}
	cmdPgModeler := exec.Command("docker", "run", "--rm", "-v", fmt.Sprintf("%s:%s", input, input), "-v", fmt.Sprintf("%s:%s:Z", output, output), PGMODELER_CLI_DOCKER_IMAGE, "pgmodeler-cli", "--input", input, "--export-to-file", "--output", output)
	cmdPgModeler.Stderr = os.Stderr
	cmdPgModeler.Stdout = os.Stdout
	return cmdPgModeler.Run()
}

func PgModelerExportToPng(input, output string) error {
	err := ioutil.WriteFile(output, []byte{}, 0755)
	if err != nil {
		return err
	}
	cmdPgModeler := exec.Command("docker", "run", "--rm", "-v", fmt.Sprintf("%s:%s", input, input), "-v", fmt.Sprintf("%s:%s:Z", output, output), PGMODELER_CLI_DOCKER_IMAGE, "pgmodeler-cli", "--input", input, "--export-to-png", "--output", output)
	cmdPgModeler.Stderr = os.Stderr
	cmdPgModeler.Stdout = os.Stdout
	return cmdPgModeler.Run()
}
