package internal

import (
	"os"
	"os/exec"
	"strings"
)

const POSTGRES_DOCKER_IMAGE = "postgres:13"

func DockerRunPostgresContainer() (string, error) {
	cmdDocker := exec.Command("docker", "run", "--rm", "-d", "-e", "POSTGRES_PASSWORD=postgres", POSTGRES_DOCKER_IMAGE)
	cmdDocker.Stderr = os.Stderr
	containerID, err := cmdDocker.Output()
	return strings.ReplaceAll(string(containerID), "\n", ""), err
}

func DockerKillContainer(containerID string) {
	cmdDocker := exec.Command("docker", "kill", containerID)
	cmdDocker.Run()
}

func DockerGetContainerIP(containerID string) (string, error) {
	cmdDocker := exec.Command("docker", "inspect", "-f", "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}", containerID)
	cmdDocker.Stderr = os.Stderr
	output, err := cmdDocker.Output()
	if err != nil {
		return "", err
	}
	return strings.TrimSuffix(string(output), "\n"), nil
}
