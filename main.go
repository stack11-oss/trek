package main

import (
	"gitlab.com/stack11-oss/trek/cmd"
)

func main() {
	cmd.Execute()
}
