package cmd

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/fsnotify/fsnotify"
	"github.com/golang-migrate/migrate/v4"
	"github.com/spf13/cobra"
	"github.com/thecodeteam/goodbye"

	"gitlab.com/stack11-oss/trek/internal"
)

var (
	flagDiffInitial bool
	flagOnce        bool
)

var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "Generate the migrations for a pgModeler file",
	Args: func(cmd *cobra.Command, args []string) error {
		if !flagDiffInitial && len(args) != 1 {
			return errors.New("pass the name of the migration or use the -i/--initial flag for the initial migration")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		internal.AssertGenerateToolsAvailable()

		config, err := internal.ReadConfig()
		if err != nil {
			log.Fatalf("Failed to read config: %v\n", err)
		}

		ctx := context.Background()
		defer goodbye.Exit(ctx, -1)
		goodbye.Notify(ctx)
		goodbye.Register(func(ctx context.Context, sig os.Signal) {
			internal.DockerKillContainer(targetContainerID)
			internal.DockerKillContainer(migrateContainerID)
		})

		updateDiff(*config, args)

		if !flagOnce {
			watcher, err := fsnotify.NewWatcher()
			if err != nil {
				log.Fatalf("Failed to create fsnotify watcher: %v\n", err)
			}
			defer watcher.Close()

			done := make(chan bool)

			go func() {
			Loop:
				for {
					select {
					case <-watcher.Events:
						updateDiff(*config, args)
					case err := <-watcher.Errors:
						log.Println(err)
						break Loop
					}
				}
				close(done)
			}()

			wd, err := os.Getwd()
			if err != nil {
				log.Fatalf("Failed to get working directory: %v\n", err)
			}

			err = watcher.Add(filepath.Join(wd, fmt.Sprintf("%s.dbm", config.ModelName)))
			if err != nil {
				log.Fatalf("Failed to watch model file: %v\n", err)
			}

			<-done
		}
	},
}

func init() {
	generateCmd.Flags().BoolVarP(&flagDiffInitial, "initial", "i", false, "Directly copy the diff to the migrations. Used for first time setup")
	generateCmd.Flags().BoolVar(&flagOnce, "once", false, "Run only once and don't watch files")
	rootCmd.AddCommand(generateCmd)
}

var (
	modelContent       = ""
	targetContainerID  = ""
	migrateContainerID = ""
)

func updateDiff(config internal.Config, args []string) {
	wd, err := os.Getwd()
	if err != nil {
		log.Fatalf("Failed to get working directory: %v\n", err)
	}

	m, err := ioutil.ReadFile(filepath.Join(wd, fmt.Sprintf("%s.dbm", config.ModelName)))
	if err != nil {
		log.Fatalln(err)
	}
	mstr := strings.TrimSuffix(string(m), "\n")
	if len(mstr) == 0 || mstr == modelContent {
		return
	}
	modelContent = mstr

	migrationsDir := filepath.Join(wd, "migrations")
	if _, err := os.Stat(migrationsDir); os.IsNotExist(err) {
		err = os.Mkdir(migrationsDir, 0755)
		if err != nil {
			log.Fatalf("Failed to create migrations directory: %v\n", err)
		}
	}

	targetContainerID, err = internal.DockerRunPostgresContainer()
	if err != nil {
		log.Fatalf("Failed to create target container: %v\n", err)
	}
	migrateContainerID, err = internal.DockerRunPostgresContainer()
	if err != nil {
		log.Fatalf("Failed to create migrate container: %v\n", err)
	}

	defer func() {
		internal.DockerKillContainer(targetContainerID)
		internal.DockerKillContainer(migrateContainerID)
	}()

	go func() {
		err := internal.PgModelerExportToPng(filepath.Join(wd, fmt.Sprintf("%s.dbm", config.ModelName)), filepath.Join(wd, fmt.Sprintf("%s.png", config.ModelName)))
		if err != nil {
			log.Fatalln(err)
		}
	}()

	err = internal.PgModelerExportToFile(filepath.Join(wd, fmt.Sprintf("%s.dbm", config.ModelName)), filepath.Join(wd, fmt.Sprintf("%s.sql", config.ModelName)))
	if err != nil {
		log.Fatalln(err)
	}

	if flagDiffInitial {
		// If we are developing the schema initially, there will be no diffs and we want to copy over the schema file to the initial migration file
		input, err := ioutil.ReadFile(filepath.Join(wd, fmt.Sprintf("%s.sql", config.ModelName)))
		if err != nil {
			log.Fatalln(err)
		}
		err = ioutil.WriteFile(filepath.Join(wd, "migrations", "001_init.up.sql"), input, 0755)
		if err != nil {
			log.Fatalln(err)
		}
	} else {
		migrationName := args[0]

		migrateDSN, err := setupMigrateDatabase(wd, config, migrationName, migrateContainerID)
		if err != nil {
			log.Fatalln(err)
		}

		targetDSN, err := setupTargetDatabase(wd, config, targetContainerID)
		if err != nil {
			log.Fatalln(err)
		}

		diff, err := internal.Migra(migrateDSN, targetDSN)
		if err != nil {
			log.Fatalln(err)
		}

		// Filter stuff from go-migrate that doesn't exist in the target db and we don't have and need anyway
		diff = strings.ReplaceAll(diff, "alter table \"public\".\"schema_migrations\" drop constraint \"schema_migrations_pkey\";", "")
		diff = strings.ReplaceAll(diff, "drop index if exists \"public\".\"schema_migrations_pkey\";", "")
		diff = strings.ReplaceAll(diff, "drop table \"public\".\"schema_migrations\";", "")
		diff = strings.Trim(diff, "\n")

		err = ioutil.WriteFile(filepath.Join(wd, "migrations", fmt.Sprintf("%s.up.sql", migrationName)), []byte(diff), 0755)
		if err != nil {
			log.Fatalln(err)
		}
	}
}

func setupMigrateDatabase(wd string, config internal.Config, migrationName, migrateContainerID string) (string, error) {
	targetIP, err := setupDatabase(migrateContainerID, config)
	if err != nil {
		return "", err
	}

	migrationFile := filepath.Join(wd, "migrations", fmt.Sprintf("%s.up.sql", migrationName))
	if _, err := os.Stat(migrationFile); err == nil {
		err = os.Remove(migrationFile)
		if err != nil {
			return "", err
		}
	}

	migrateDSN := fmt.Sprintf("postgresql://postgres:postgres@%s:5432/%s?sslmode=disable", targetIP, config.DatabaseName)
	m, err := migrate.New(fmt.Sprintf("file://%s", filepath.Join(wd, "migrations")), migrateDSN)
	if err != nil {
		return "", err
	}
	err = m.Up()
	if err != nil {
		return "", err
	}

	return migrateDSN, nil
}

func setupTargetDatabase(wd string, config internal.Config, targetContainerID string) (string, error) {
	targetIP, err := setupDatabase(targetContainerID, config)
	if err != nil {
		return "", err
	}

	err = internal.PsqlFile(targetIP, internal.PGDefaultUsername, internal.PGDefaultPassword, "disable", config.DatabaseName, filepath.Join(wd, fmt.Sprintf("%s.sql", config.ModelName)))
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("postgresql://postgres:postgres@%s:5432/%s?sslmode=disable", targetIP, config.DatabaseName), nil
}

func setupDatabase(containerName string, config internal.Config) (containerIP string, err error) {
	ip, err := internal.DockerGetContainerIP(containerName)
	if err != nil {
		return "", err
	}

	internal.PsqlWaitDatabaseUp(ip, internal.PGDefaultUsername, internal.PGDefaultPassword, "disable")
	err = internal.PsqlHelperSetupDatabaseAndUsers(ip, internal.PGDefaultUsername, internal.PGDefaultPassword, "disable", config.DatabaseName, config.DatabaseUsers)

	return ip, err
}
